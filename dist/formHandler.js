// General Settings
var msgElement = "pageBlockMessage";
var defaultSpinner = "icon-loop3";
var defaultDelay = 3000;


// DO NOT CHANGE BELOW THIS LINE
// ------------------------------
var timeouts = [];
$( "form" ).on( "submit", function( e ) {
    e.preventDefault();
    var form = $(this);

    defaultDelay = $(form).data("delay") != null ? parseInt($(form).data("delay")): 3000;
    var formAction = form.attr('action');
    var data = form.serialize();
    var blockMessages = $(form).data("messages") != null ? $(form).data("messages"): "Please Wait";
    var blockIcon = $(form).data("icon") != null ? $(form).data("icon"): "icon-loop3";
    var successHandler = $(form).data("successhandler");
    var successMsg = $(form).data("successmsg");
    var validationFunction = $(form).data("validator");

    blockPage(msgElement, blockMessages, blockIcon);
    
    if(validationFunction != null)
    {
        if(executeFunctionByName(validationFunction, window) === false)
        {
            unBlockPage();
            return;
        }
    }

    try 
    {
        var token = $('input[name="__RequestVerificationToken"]').val();
        var headers = {};
        headers['__RequestVerificationToken'] = token;
        
        

        $.ajax({
            type: "POST",
            url: formAction,
            headers: headers,
            data: data
          })
        .done(function(result) {
            clearTimeouts(timeouts);
            changeMessage("pageBlockDiv", successMsg);
            unBlockPage(1500);
            if(successHandler != null)
            {
                executeFunctionByName(successHandler, window, result)
            }
            return;
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log("Error: " + errorThrown);
            clearTimeouts(timeouts);
            changeMessage("pageBlockDiv", "An unhandled error has occured...");
            unBlockPage(3000);
            return;
        });;
    
    }
    catch (e) 
    {
        console.log("Error: " + e);
        clearTimeouts(timeouts);
        changeMessage("pageBlockDiv", "An unhandled error has occured...");
        unBlockPage(3000);
        return;
    }
});

function blockPage(msgElement, messages, spinnerIcon)
{
    msgArray = messages.split(',');
    $.blockUI({ 
        message: '<div id="pageBlockDiv"></div>',
        overlayCSS: {
            backgroundColor: '#1b2024',
            opacity: 0.8,
            zIndex: 1200,
            cursor: 'wait'
        },
        css: {
            border: 0,
            color: '#fff',
            padding: 0,
            zIndex: 1201,
            backgroundColor: 'transparent'
        }
    });

    changeMessage("pageBlockDiv", '<i class="' + defaultSpinner + ' spinner"></i><br/><span id="' + msgElement + '" class="pageBlockContent">' + msgArray[0] + '</span>', 200);

    if(msgArray.length > 1)
    {
        var changeTimer = defaultDelay;

        for (var i=0; i<msgArray.length; i++) {
            (function(i) {
                if(i != 0)
                {
                    changeMessage(msgElement, msgArray[i], changeTimer);
                    changeTimer = changeTimer + defaultDelay;
                }
            })(i);
        }
    } 
}

function unBlockPage(delay=null)
{
    if(delay != null)
    {
        timeouts.push(window.setTimeout(function () {
            $.unblockUI();
        }, delay));
    }
    else
    {
        $.unblockUI();
    }
}

function changeMessage(el, msg, delay=null)
{  
    if(delay != null)
    {
        timeouts.push(window.setTimeout(function () 
        {
            $("#" + el).fadeOut(200, function() {
                $("#" + el).html(msg).fadeIn();
            });
        }, delay));
    }
    else
    {
        $("#" + el).fadeOut(200, function() {
            $("#" + el).html(msg).fadeIn();
        });
    }
}

function clearTimeouts()
{
    for (var i = 0; i < timeouts.length; i++) 
    {
        clearTimeout(timeouts[i]);
    }
    timeouts = [];
}

function executeFunctionByName(functionName, context, args) 
{
    var args = Array.prototype.slice.call(arguments, 2);
    var namespaces = functionName.split(".");
    var func = namespaces.pop();
    for(var i = 0; i < namespaces.length; i++) 
    {
      context = context[namespaces[i]];
    }
    return context[func].apply(context, args);
}
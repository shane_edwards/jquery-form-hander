# JQuery Form Manager

The JQuery Form Manager is a simple set of Javascript that globally manages the handling of all forms on a site.  

# Dependencies

  - JQuery 3.x or higher
  - BlockUI (http://malsup.com/jquery/block/)


# Installation
  - Install both the formHandler.js and formHandler.css into your site
 
# Options
  - Change the variables at the top of formHandler.js to suit your needs
  - Adjust the CSS as needed